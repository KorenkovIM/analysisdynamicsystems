using System.Numerics;

namespace ADS.Core.Math.Test.TestClass;

public class PastDynamicSystem: DynamicSystem
{
    public PastDynamicSystem() 
        : base(nameof(PastDynamicSystem)) { }

    public override float Fx(Vector3 vector) => vector.X;
    public override float Fy(Vector3 vector) => vector.Y;
    public override float Fz(Vector3 vector) => vector.Z;
    public override Vector3 GetStartVector()
    {
        throw new NotImplementedException();
    }
}