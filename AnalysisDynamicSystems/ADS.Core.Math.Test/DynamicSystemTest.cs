using System.Numerics;
using ADS.Core.Math.Test.TestClass;
using Moq;

namespace ADS.Core.Math.Test;

[TestClass]
public class DynamicSystemTest
{
    [TestMethod]
    public void GetNextVector_PastDynamicSystem_RungeCutta_Exp()
    {
        // Arrange
        var dynamicSystem = new PastDynamicSystem();
        var vector = new Vector3(1);
        var countIteration = 100_000;
        var steap = 1f / countIteration;
        var eps = 0.001f;
        var result = System.Math.E;

        // Act
        for (int i = 0; i < countIteration; i++)
        {
            vector = dynamicSystem.GetNextVector(vector, steap);
        }

        // Assert
        Assert.IsTrue(System.Math.Abs(result - vector.X) < eps);
    }
    
    [TestMethod]
    public void GetNextVector_PastDynamicSystem_Gause_Exp()
    {
        // Arrange
        var dynamicSystem = new PastDynamicSystem();
        var vector = new Vector3(1);
        var countIteration = 100_000;
        var steap = 1f / countIteration;
        var eps = 0.001f;
        var result = System.Math.E;

        // Act
        for (int i = 0; i < countIteration; i++)
        {
            vector = dynamicSystem.GetNextVector(vector, steap, IntegrationMethodEnum.MethodGause);
        }

        // Assert
        Assert.IsTrue(System.Math.Abs(result - vector.X) < eps);
    }
}