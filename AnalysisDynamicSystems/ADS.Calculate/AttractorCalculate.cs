using System.Numerics;
using ADS.Core.Math;

namespace ADS.Calculate;

public abstract class AttractorCalculate<TPoint>: Calculate<AttractorParametrs, AttractorResult<TPoint>>
{
    public AttractorCalculate(DynamicSystem dynamicSystem) 
        : base(dynamicSystem) { }

    public override AttractorResult<TPoint> Act(AttractorParametrs parametrs)
    {
        var result = new AttractorResult<TPoint>();
        result.MaxX = Double.MinValue;
        result.MaxY = Double.MinValue;
        result.MaxZ = Double.MinValue;
        
        result.MinX = Double.MaxValue;
        result.MinY = Double.MaxValue;
        result.MinZ = Double.MaxValue;

        result.Trajectory = new List<TPoint>();

        var vector = CurrentDynamicSystem.GetStartVector();

        for (int i = 0; i < parametrs.CountIntegration; i++)
        {
            result.Trajectory.Add(GetNextIntegration(ref vector, parametrs));

            if (vector.X > result.MaxX) result.MaxX = vector.X;
            if (vector.Y > result.MaxY) result.MaxY = vector.Y;
            if (vector.Z > result.MaxZ) result.MaxZ = vector.Z;
            if (vector.X < result.MinX) result.MinX = vector.X;
            if (vector.Y < result.MinY) result.MinY = vector.Y;
            if (vector.Z < result.MinZ) result.MinZ = vector.Z;
        }

        return result;
    }

    public abstract TPoint GetNextIntegration(ref Vector3 vector, AttractorParametrs parametrs);
}