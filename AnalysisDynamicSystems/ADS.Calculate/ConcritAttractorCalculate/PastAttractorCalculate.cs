using System.Numerics;
using ADS.Core.Math;

namespace ADS.Calculate.ConcritAttractorCalculate;

public class PastAttractorCalculate: AttractorCalculate<Vector3>
{
    public PastAttractorCalculate(DynamicSystem dynamicSystem) 
        : base(dynamicSystem) { }

    public override Vector3 GetNextIntegration(ref Vector3 vector, AttractorParametrs parametrs)
    {
        vector = CurrentDynamicSystem.GetNextVector(vector, parametrs.Steap);
        return vector;
    }
}