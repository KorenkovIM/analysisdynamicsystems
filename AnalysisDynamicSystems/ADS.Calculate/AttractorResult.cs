namespace ADS.Calculate;

public class AttractorResult<TPoint>
{
    public double MaxX { get; set; }
    public double MaxY { get; set; }
    public double MaxZ { get; set; }
    public double MinX { get; set; }
    public double MinY { get; set; }
    public double MinZ { get; set; }
    
    public List<TPoint> Trajectory { get; set; }
    
}