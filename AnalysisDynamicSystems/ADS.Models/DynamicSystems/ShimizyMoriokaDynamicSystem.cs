using System.Numerics;
using ADS.Core.Math;

namespace ADS.Models.DynamicSystems;

public class ShimizyMoriokaDynamicSystem: DynamicSystem
{
    private const string NAME = "Система Шимицу-Мориока";

    public const string 
        LAMBDA = nameof(LAMBDA), 
        ALPHA = nameof(ALPHA);

    public ShimizyMoriokaDynamicSystem()
        : base(NAME)
    {
        Parametrs[LAMBDA] = 1f;
        Parametrs[ALPHA] = 0.8f;
    }

    public override float Fx(Vector3 vector) => vector.Y;
    public override float Fy(Vector3 vector) => vector.X - this[LAMBDA] * vector.Y - vector.X * vector.Z;
    public override float Fz(Vector3 vector) => -this[ALPHA] * vector.Z + vector.X * vector.X;

    public override Vector3 GetStartVector()
    {
        return new Vector3(0.1f, 0, 0);
    }
}