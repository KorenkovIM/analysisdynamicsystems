﻿// See https://aka.ms/new-console-template for more information

using ADS.Calculate;
using ADS.Calculate.ConcritAttractorCalculate;
using ADS.Models.DynamicSystems;

var a = new PastAttractorCalculate(new ShimizyMoriokaDynamicSystem());

var b = a.Act(new AttractorParametrs
{
    CountIntegration = 1_000_000,
    Steap = 0.01f,
});

Console.WriteLine(b);