namespace ADS.Core.Math;

public abstract class Calculate<TParams, TOut>
{
    public DynamicSystem CurrentDynamicSystem { get; }
    public abstract TOut Act(TParams parametrs);
    public Calculate(DynamicSystem dynamicSystem)
    {
        CurrentDynamicSystem = dynamicSystem;
    }
}