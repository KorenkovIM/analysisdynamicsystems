using SixLabors.ImageSharp;

namespace ADS.Core.Math;

public abstract class Mapper<TOut>
{
    public abstract Color[,] Mapping(TOut result);
}