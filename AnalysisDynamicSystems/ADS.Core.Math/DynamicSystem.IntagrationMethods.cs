using System.Numerics;

namespace ADS.Core.Math;

public abstract partial class DynamicSystem
{
    private Vector3 MethodRungeCutta(Vector3 vector, float steap)
    {
        if (
            float.IsNaN(steap) ||
            float.IsInfinity(steap) ||
            steap >= 10 ||
            steap < 0.00000001f) 
            throw new Exception();

        float[,] k = new float[4, 3];

        k[0, 0] = Fx(vector);
        k[0, 1] = Fy(vector);
        k[0, 2] = Fz(vector);

        k[1, 0] = Fx(new Vector3(vector.X + steap / 2, vector.Y + steap / 2 * k[0, 1], vector.Z + steap / 2 * k[0, 2]));
        k[1, 1] = Fy(new Vector3(vector.X + steap / 2 * k[0, 0], vector.Y + steap / 2, vector.Z + steap / 2 * k[0, 2]));
        k[1, 2] = Fz(new Vector3(vector.X + steap / 2 * k[0, 0], vector.Y + steap / 2 * k[0, 1], vector.Z + steap / 2));

        k[2, 0] = Fx(new Vector3(vector.X + steap / 2, vector.Y + steap / 2 * k[1, 1], vector.Z + steap / 2 * k[1, 2]));
        k[2, 1] = Fy(new Vector3(vector.X + steap / 2 * k[1, 0], vector.Y + steap / 2, vector.Z + steap / 2 * k[1, 2]));
        k[2, 2] = Fz(new Vector3(vector.X + steap / 2 * k[1, 0], vector.Y + steap / 2 * k[1, 1], vector.Z + steap / 2));

        k[3, 0] = Fx(new Vector3(vector.X + steap, vector.Y + steap * k[2, 1], vector.Z + steap * k[2, 2]));
        k[3, 1] = Fy(new Vector3(vector.X + steap * k[2, 0], vector.Y + steap, vector.Z + steap * k[2, 2]));
        k[3, 2] = Fz(new Vector3(vector.X + steap * k[2, 0], vector.Y + steap * k[2, 1], vector.Z + steap));

        vector.X += steap / 6 * (k[0, 0] + 2 * k[1, 0] + 2 * k[2, 0] + k[3, 0]);
        vector.Y += steap / 6 * (k[0, 1] + 2 * k[1, 1] + 2 * k[2, 1] + k[3, 1]);
        vector.Z += steap / 6 * (k[0, 2] + 2 * k[1, 2] + 2 * k[2, 2] + k[3, 2]);

        return vector;
    }

    public Vector3 MethodGause(Vector3 vector, float steap)
    {
        return vector + new Vector3
        (
            Fx(vector) * steap, 
            Fy(vector) * steap, 
            Fz(vector) * steap
        );
    }
}