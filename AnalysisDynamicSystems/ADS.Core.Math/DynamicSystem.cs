using System.Numerics;

namespace ADS.Core.Math;

public abstract partial class DynamicSystem
{
    public string Name { get; private set; }

    protected Dictionary<string, float> Parametrs;
    public float this[string parametrName]
    {
        get
        {
            if (Parametrs.TryGetValue(parametrName, out var data))
            {
                return data;
            }
            else
            {
                throw new Exception($"У системы {Name} нет параметра {parametrName}");
            }
        }
    }

    public abstract float Fx(Vector3 vector);
    public abstract float Fy(Vector3 vector);
    public abstract float Fz(Vector3 vector);

    public Vector3 GetNextVector(Vector3 vector, float setap, IntegrationMethodEnum method)
    {
        return method switch
        {
            IntegrationMethodEnum.MethodGause => MethodGause(vector, setap),
            IntegrationMethodEnum.MethodRungeKutta => MethodRungeCutta(vector, setap),
            _ => throw new Exception()
        };
    }
    public Vector3 GetNextVector(Vector3 vector, float setap) =>
        GetNextVector(vector, setap, IntegrationMethodEnum.MethodRungeKutta);
    
    public abstract Vector3 GetStartVector();
    
    public DynamicSystem(string name)
    {
        Parametrs = new();
        Name = name;
    }
}