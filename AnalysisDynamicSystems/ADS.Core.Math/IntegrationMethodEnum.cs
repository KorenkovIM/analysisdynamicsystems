namespace ADS.Core.Math;

public enum IntegrationMethodEnum
{
    MethodGause,
    MethodRungeKutta,
}