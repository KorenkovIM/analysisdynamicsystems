namespace ADS.Core.Math;

public class Parametr
{
    public string Name { get; set; }
    public float Value { get; set; }
}